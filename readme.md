## Labopart telegram bot
An OCI Container environment for automated SARS-CoV-2 test result retrieval via Selenium w/ Firefox headless. The result is sent to your Telegram Bot which you can easily configure or create via Telegram's [Bot Father](https://t.me/botfather).

Requires `buildah` and `podman`. Alternatively, you can use `docker build` and `docker run`, respectively.

* Replace `<ORDERNR>`, `<BIRTHDAY>`, `<BOT_TOKEN>` and `<CHAT_ID>` in the command below
  * `<ORDERNR>` is the Labopart Order Number
  * `<BIRTHDAY>` must be in the form `yyyy-mm-dd`
  * use the `<BOT_TOKEN>` from the Telegram Bot Father
  * to get a `<CHAT_ID>` read up on [StackOverflow](https://stackoverflow.com/questions/32423837/telegram-bot-how-to-get-a-group-chat-id#32572159)

```bash
buildah bud -t labopart-bot

# add this command to your cronjobs for e.g., hourly updates
podman run \
  --env ORDERNR=<ORDERNR> \
  --env BIRTHDAY=<BIRTHDAY> \
  --env TELEGRAM_BOT_TOKEN="<BOT_TOKEN>" \
  --env TELEGRAM_CHAT_ID=<CHAT_ID> \
  labopart-bot
```
