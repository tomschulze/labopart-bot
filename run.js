const {Builder, By, Key, until} = require('selenium-webdriver');
const firefox = require('selenium-webdriver/firefox');
const TelegramBot = require('node-telegram-bot-api');

const token = process.env.TELEGRAM_BOT_TOKEN;
const chatId = process.env.TELEGRAM_CHAT_ID;
const ordernr = process.env.ORDERNR;
const birthday = process.env.BIRTHDAY;

async function getStatus() {
  let driver = await new Builder().forBrowser('firefox').setFirefoxOptions(new firefox.Options().headless()).build();

  await driver.get('https://com.labopart.de');
  await driver.findElement(By.id('ordernumber')).sendKeys(ordernr);
  await driver.findElement(By.id('birthdate')).sendKeys(birthday);
  // implicitly wait until element is clickable
  const button = await driver.wait(until.elementLocated(By.tagName('button')), 30000, 'Timeout', 5000);
  await button.click();
  let elem = await driver.wait(until.elementLocated(By.css('.c19ba-header_title-text')),30000,'Timeout',5000);
  const text = await elem.getAttribute('textContent');

  if ( text === 'COVID-19 Sofortauskunft') {
    await driver.quit();
    return 'Keine Befunddaten gefunden.';
  }

  await driver.quit();
  return text;
}

async function send() {
  const bot = new TelegramBot(token);
  const msg = await getStatus();
  bot.sendMessage(chatId, msg);
}

send();
