FROM node:fermium-alpine3.10

ARG GECKODRIVER_VERSION=0.27.0

# enable alpine community repo to install firefox-esl
RUN echo "http://dl-cdn.alpinelinux.org/alpine/edge/community" >> /etc/apk/repositories
RUN apk --update add wget firefox-esr

RUN npm install selenium-webdriver node-telegram-bot-api

RUN wget -q https://github.com/mozilla/geckodriver/releases/download/v$GECKODRIVER_VERSION/geckodriver-v$GECKODRIVER_VERSION-linux64.tar.gz && \
  tar -xf geckodriver-v$GECKODRIVER_VERSION-linux64.tar.gz -C /usr/bin

COPY run.js /run.js
CMD ["node", "/run.js"]
